from typing import Any
from typing import List

import models.crud as crud
import schemas
from api_v1.utils import get_db
from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from models.article import Article
from models.article import ArticleComment
from sqlalchemy.orm import Session

router = APIRouter()


@router.get("/{id}", response_model=schemas.Article)
def get_article(
    *,
    db: Session = Depends(get_db),
    id: int,
) -> Any:
    """
    Get article by ID.
    """
    article: Article = crud.article.get(db=db, id=id)
    if not article:
        raise HTTPException(status_code=404, detail="Article not found")
    result: Any = jsonable_encoder(article)

    return JSONResponse(content=result)


@router.get("/", response_model=List[schemas.Article])
def get_article_list(
    *,
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100,
) -> Any:
    """
    Get articles list.
    """
    articles: List[Article] = crud.article.get_multi(db=db, skip=skip, limit=limit)
    if not articles:
        raise HTTPException(
            status_code=404, detail="Comments for this article not found"
        )
    result: Any = jsonable_encoder(articles)

    return JSONResponse(content=result)


@router.delete("/{id}", response_model=schemas.Article)
def delete_article(
    *,
    db: Session = Depends(get_db),
    id: int,
) -> Any:
    """
    Delete an article.
    """
    article: Article = crud.article.get(db=db, id=id)
    if not article:
        raise HTTPException(status_code=404, detail="Article not found")

    article: Article = crud.article.remove(db=db, id=id)
    result: Any = jsonable_encoder(article)

    return JSONResponse(content=result)


@router.get("/{id}/comments", response_model=List[schemas.ArticleComment])
def get_article_comments(
    *,
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100,
    id: int,
) -> Any:
    """
    Get comments by article ID.
    """
    comments: List[ArticleComment] = (
        db.query(ArticleComment)
        .where(ArticleComment.article_id == id)
        .offset(skip)
        .limit(limit)
        .all()
    )
    if not comments:
        raise HTTPException(
            status_code=404, detail="Comments for this article not found"
        )
    result: Any = jsonable_encoder(comments)

    return JSONResponse(content=result)
