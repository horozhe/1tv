from api_v1.endpoints import articles
from fastapi import APIRouter

api_router: APIRouter = APIRouter()
api_router.include_router(articles.router, prefix="/articles", tags=["articles"])
