from models.article import Article
from models.article import ArticleComment
from models.crud.base import CRUDBase


class CRUDArticle(CRUDBase):
    pass


class CRUDArticleComment(CRUDBase):
    pass


article = CRUDArticle(Article)
article_comment = CRUDArticleComment(ArticleComment)
