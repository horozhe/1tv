from db.base_class import Base
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import relationship


class Article(Base):
    __tablename__ = "articles"

    id: Mapped[int] = Column(Integer, primary_key=True, index=True, autoincrement=True)
    text: Mapped[str] = Column(Text)


class ArticleComment(Base):
    __tablename__ = "article_comments"

    id: Mapped[int] = Column(Integer, primary_key=True, index=True, autoincrement=True)
    article_id: Mapped[int] = Column(Integer, ForeignKey("articles.id"))
    article: Mapped["Article"] = relationship("Article", cascade="all, delete")
    author: Mapped[str] = Column(String, index=True)
    content: Mapped[str] = Column(Text)
