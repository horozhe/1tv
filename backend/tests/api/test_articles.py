from core.config import settings
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from tests.utils.article import create_random_article
from tests.utils.article import create_random_article_comment

"""
По-хорошему бы ещё нужна отдельная тестовая база и очистка мусора после каждого тест-кейса
"""


def test_get_article(client: TestClient, db: Session) -> None:
    article = create_random_article(db)

    response = client.get(
        f"{settings.API_V1_STR}/articles/{article.id}",
    )

    assert response.status_code == 200
    content = response.json()
    assert content["id"] == article.id
    assert content["text"] == article.text


def test_get_article_list(client: TestClient, db: Session) -> None:
    for _ in range(2):
        create_random_article(db)

    response = client.get(
        f"{settings.API_V1_STR}/articles/",
    )

    assert response.status_code == 200
    content = response.json()
    assert len(content) > 1


def test_delete_article(client: TestClient, db: Session) -> None:
    article = create_random_article(db)

    response = client.delete(
        f"{settings.API_V1_STR}/articles/{article.id}",
    )

    assert response.status_code == 200

    response = client.get(
        f"{settings.API_V1_STR}/articles/{article.id}",
    )

    assert response.status_code == 404


def test_get_article_comments(client: TestClient, db: Session) -> None:
    article = create_random_article(db)

    create_random_article_comment(db, article.id)
    create_random_article_comment(db, article.id)
    create_random_article_comment(db, article.id)

    response = client.get(
        f"{settings.API_V1_STR}/articles/{article.id}/comments",
    )

    assert response.status_code == 200

    content = response.json()

    assert len(content) == 3
