from models.article import Article
from models.crud import article_crud
from schemas.article import ArticleCreate
from schemas.article_comment import ArticleCommentCreate
from sqlalchemy.orm import Session
from tests.utils.base import random_lower_string


def create_random_article(db: Session) -> Article:
    text = random_lower_string()
    item_in = ArticleCreate(text=text)

    return article_crud.article.create(db=db, obj_in=item_in)


def create_random_article_comment(db: Session, article_id: int) -> Article:
    article_id: int = article_id
    author: str = random_lower_string()
    content: str = random_lower_string()
    item_in = ArticleCommentCreate(
        article_id=article_id, author=author, content=content
    )

    return article_crud.article_comment.create(db=db, obj_in=item_in)
