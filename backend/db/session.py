from typing import Any

from core.config import settings
from sqlalchemy import Engine
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine: Engine = create_engine(settings.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True)
SessionLocal: Any = sessionmaker(autocommit=False, autoflush=False, bind=engine)
