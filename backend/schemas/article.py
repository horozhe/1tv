from typing import Optional

from pydantic import BaseModel


# Shared properties
class Article(BaseModel):
    id: int
    text: Optional[str] = None


# Properties to receive on item creation
class ArticleCreate(Article):
    id: Optional[int]
    text: Optional[str] = None
