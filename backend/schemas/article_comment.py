from typing import Optional

from pydantic import BaseModel


class ArticleComment(BaseModel):
    id: int
    article_id: int
    author: str
    content: str


class ArticleCommentCreate(BaseModel):
    id: Optional[int]
    article_id: int
    author: str
    content: str
