#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

# Run migrations
poetry run alembic upgrade head

# Run server
poetry run uvicorn main:app --reload --host 0.0.0.0 --port 80
