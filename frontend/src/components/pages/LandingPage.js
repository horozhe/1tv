import React from 'react'
import { Link } from 'react-router-dom'

import '../../App.css'

export default function LandingPage() {
    return (
        <header style={ HeaderStyle }>
            <h1 align={"center"}>1tv test task</h1>
            <div className="buttons text-center">
                <Link to="/login">
                    <button className="primary-button">log in</button>
                </Link>
            </div>
        </header>
    )
}

const HeaderStyle = {
    width: "100%",
    height: "100vh",
    backgroundColor: `white`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"
}