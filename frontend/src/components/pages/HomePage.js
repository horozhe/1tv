import React from 'react'
import {Link} from 'react-router-dom'
import Box from '@mui/material/Box';
import {DataGrid, GridColDef, GridValueGetterParams} from '@mui/x-data-grid';

const columns: GridColDef[] = [
    {field: 'id', headerName: 'ID', width: 90},
    {
        field: 'text',
        headerName: 'Article text',
        description: 'This column has a value getter and is not sortable.',
        sortable: false,
        width: 160,
        valueGetter: (params: GridValueGetterParams) =>
            `${params.row.firstName || ''} ${params.row.lastName || ''}`,
    },
];

// const App = () => {
//    const [posts, setPosts] = useState([]);
//    useEffect(() => {
//       fetch('https://localhost/api/v1/articles')
//          .then((response) => response.json())
//          .then((data) => {
//             console.log(data);
//             setPosts(data);
//          })
//          .catch((err) => {
//             console.log(err.message);
//          });
//    }, []);

const rows = [
    {id: 1, text: 'Test text 1'},
    {id: 2, text: 'Test text 2'},
    {id: 3, text: 'Test text 3'},
    {id: 4, text: 'Test text 4'},
    {id: 5, text: 'Test text 5'},
    {id: 6, text: 'Test text 6'},
    {id: 7, text: 'Test text 7'},
    {id: 8, text: 'Test text 8'},
    {id: 9, text: 'Test text 9'},
];

export default function HomePage() {
    return (
        <div className="text-center">
            <Box sx={{height: 400, width: '100%'}}>
                <DataGrid
                    rows={rows}
                    columns={columns}
                    initialState={{
                        pagination: {
                            paginationModel: {
                                pageSize: 5,
                            },
                        },
                    }}
                    pageSizeOptions={[5]}
                    checkboxSelection
                    disableRowSelectionOnClick
                />
            </Box>
            <Link to="/">
                <button className="primary-button">Log out</button>
            </Link>
        </div>
    )
}

